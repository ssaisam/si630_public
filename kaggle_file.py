import csv
import pickle
from scipy import spatial
import numpy as np

file = open(r"intrinsic-test.csv","r") .readlines()[1:]

W1 = pickle.load(open("w1_v3.p","rb"))
W2 = pickle.load( open("w2_v3.p","rb"))
w2v_uniqueWords = pickle.load( open("w2v_uniqueWords.p","rb"))

def compare(word1,word2):
    word1_index = W1[w2v_uniqueWords.index(word1)]
    word2_index = W1[w2v_uniqueWords.index(word2)]
    return 1 - spatial.distance.cosine(word1_index, word2_index)

# with open('emailoutputs.csv', mode='w') as employee_file:
#     #Creating a write variable to help us right to the file
#     employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting = csv.QUOTE_MINIMAL)
#     employee_writer.writerow(["id","sim"])
#     for i in file:
#         i  = i.replace("\n","")
#         i = i.split(",")
#         employee_writer.writerow([i[ 0], compare(i[ 1],i[2]) ])

word_compare="good"
word1_index = W1[w2v_uniqueWords.index(word_compare)]
l=[]
for i in w2v_uniqueWords:
    word2_index = W1[w2v_uniqueWords.index(i)]
    l.append((i,1 - spatial.distance.cosine(word1_index, word2_index)))

sorted_by_second = sorted(l, key=lambda tup: tup[1] , reverse =True)
print(sorted_by_second[:10])