#!/usr/bin/env python
# coding: utf-8

# In[1]:


# import iteround
import os,sys,re,csv
import pickle
from collections import Counter, defaultdict
import numpy as np
import scipy
import math
import random
import nltk
from scipy.spatial.distance import cosine
from nltk.corpus import stopwords
from numba import jit
import string
epochs = 5
num_samples = 2
learning_rate = 0.05
nll = 0
iternum = 0

random.seed(10)
np.random.seed(10)
randcounter = 10
np_randcounter = 10

stop_words = set(stopwords.words('english'))
vocab_size = 0
hidden_size = 100
origcounts = {}
uniqueWords = [""]                      #... list of all unique tokens
wordcodes = {}                          #... dictionary mapping of words to indices in uniqueWords
wordcounts = Counter()                  #... how many times each token occurs
samplingTable = []


# In[2]:


def loadData(filename):
    global uniqueWords, wordcodes, wordcounts
    override = False
    if override:
        #... for debugging purposes, reloading input file and tokenizing is quite slow
        #...  >> simply reload the completed objects. Instantaneous.
        fullrec = pickle.load(open("w2v_fullrec.p","rb"))
        wordcodes = pickle.load( open("w2v_wordcodes.p","rb"))
        uniqueWords= pickle.load(open("w2v_uniqueWords.p","rb"))
        wordcounts = pickle.load(open("w2v_wordcounts.p","rb"))
        return fullrec
    # ... load in first 15,000 rows of unlabeled data file.  You can load in
    # more if you want later (and should do this for the final homework)
    handle = open(filename, "r", encoding="utf8")
    fullconts = handle.read().split("\n")
    fullconts = fullconts[1:15000]  # (TASK) Use all the data for the final submission
    #... apply simple tokenization (whitespace and lowercase)
    fullconts = [" ".join(fullconts).lower()]




    #CREATED
    print ("Generating token stream...")
    #... (TASK) populate fullrec as one-dimension array of all tokens in the order they appear.
    #... ignore stopwords in this process
    #... for simplicity, you may use nltk.word_tokenize() to split fullconts.
    #... keep track of the frequency counts of tokens in origcounts.
    fullrec = []

    fullrec = (([nltk.tokenize.word_tokenize(i.translate(str.maketrans('', '', string.punctuation))) for i in fullconts]))
    fullrec = [item for sublist in fullrec for item in sublist]
    fullrec = [i for i in fullrec if not i in stop_words ]
        
        
        
    # fullrec = [i for i in fullrec if not i in stop_words ]
    min_count = 50
    origcounts = dict(Counter(fullrec))
    uniqueWords = origcounts.keys()


    print ("Performing minimum thresholding..")
    #... (TASK) populate array fullrec_filtered to include terms as-is that appeared at least min_count times
    #... replace other terms with <UNK> token.
    #... update frequency count of each token in dict wordcounts where: wordcounts[token] = freq(token)
    fullrec_filtered = [i if origcounts[i] > min_count else "UNF" for i in fullrec]
    # fullrec_filtered = filter(lambda a: a!= "UNF", fullrec_filtered)
    fullrec_filtered =  list(filter(lambda a: a != "UNF", fullrec_filtered) )
    # fullrec_filtered = ["UNF" if i in stop_words else i for i in fullrec_filtered]
    wordcounts = dict(Counter(fullrec_filtered))
    uniqueWords = wordcounts.keys()
    # [wordcounts.pop(i, None) for i in stop_words ]#
    # wordcounts.pop("UNF")
    #... after filling in fullrec_filtered, replace the original fullrec with this one.
    fullrec = fullrec_filtered






    print ("Producing one-hot indicies")
    #... (TASK) sort the unique tokens into array uniqueWords
    #... produce their one-hot indices in dict wordcodes where wordcodes[token] = onehot_index(token)
    #... replace all word tokens in fullrec with their corresponding one-hot indices.
    uniqueWords = list(Counter( fullrec).keys())#... fill in
    # wordcodes = #... fill in
    # uniqueWords.remove("UNF")
    # iden = np.asmatrix(np.identity(len(uniqueWords)))
    iden = list(range(len(uniqueWords)))
    wordcodes = { k:np.transpose(v) for (k,v) in zip(uniqueWords, iden)}
    




    #... close input file handle
    handle.close()




    #... store these objects for later.
    #... for debugging, don't keep re-tokenizing same data in same way.
    #... just reload the already-processed input data with pickles.
    #... NOTE: you have to reload data from scratch if you change the min_count, tokenization or number of input rows
    
    pickle.dump(fullrec, open("w2v_fullrec.p","wb+"))
    pickle.dump(wordcodes, open("w2v_wordcodes.p","wb+"))
    pickle.dump(uniqueWords, open("w2v_uniqueWords.p","wb+"))
    pickle.dump(dict(wordcounts), open("w2v_wordcounts.p","wb+"))

    #... output fullrec should be sequence of tokens, each represented as their one-hot index from wordcodes.
    return fullrec


# In[3]:



fullrec = loadData("unlabeled-data.txt")


# In[4]:


#... stores the normalizing denominator (count of all tokens, each count raised to exp_power)
max_exp_count = 0
exp_power =0.75

print ("Generating exponentiated count vectors")
#... (TASK) for each uniqueWord, compute the frequency of that word to the power of exp_power
#... store results in exp_count_array.
sum_vals = sum(wordcounts.values())
exp_count_array =  np.asarray([((wordcounts[i]/sum_vals)** exp_power) for i in wordcounts.keys()])   #... fill in
max_exp_count = sum(exp_count_array)


print ("Generating distribution")
#... (TASK) compute the normalized probabilities of each term.
#... using exp_count_array, normalize each value by the total value max_exp_count so that
#... they all add up to 1. Store this corresponding array in prob_dist
prob_dist = exp_count_array / max_exp_count

print ("Filling up sampling table")
#... (TASK) create a dict of size table_size where each key is a sequential number and its value is a one-hot index
#... the number of sequential keys containing the same one-hot index should be proportional to its prob_dist value
#... multiplied by table_size. This table should be stored in cumulative_dict.
#... we do this for much faster lookup later on when sampling from this table.
table_size = 1e7
val  = list(wordcounts.values())
key = list(wordcounts.keys())
cumulative_list = [[i]*(int(np.round(prob_dist[i]*table_size))) for i in range(len(wordcounts))]
cumulative_list = [item for sublist in cumulative_list for item in sublist]
values = list(range(len(cumulative_list)))
cumulative_dict = { k:v for (k,v) in zip(list(values),cumulative_list)}



# In[5]:


def generateSamples(context_idx, num_samples):
    results = []
    #... (TASK) randomly sample num_samples token indices from samplingTable.
    #... don't allow the chosen token to be context_idx.
    #... append the chosen indices to results
    while(len(results)<num_samples):
        n = random.randint(0,len(cumulative_dict)-1)
#         print(n)
        if(cumulative_dict[n] != context_idx):
            results.append(cumulative_dict[n])
    return results


# In[6]:







fullsequence = fullrec

curW1=None
vocab_size = len(uniqueWords)           #... unique characters
hidden_size = 100                       #... number of hidden neurons
context_window = [-2,-1,1,2]            #... specifies which context indices are output. Indices relative to target word. Don't include index 0 itself.
nll_results = []                        #... keep array of negative log-likelihood after every 1000 iterations
nll_results_temp_all = []

#... determine how much of the full sequence we can use while still accommodating the context window
start_point = int(math.fabs(min(context_window)))
end_point = len(fullsequence)-(max(max(context_window),0))
mapped_sequence = fullsequence




#... initialize the weight matrices. W1 is from input->hidden and W2 is from hidden->output.
if curW1==None:
    np_randcounter += 1
    W1 = np.random.uniform(-.5, .5, size=(vocab_size, hidden_size))
    W2 = np.random.uniform(-.5, .5, size=(vocab_size, hidden_size))
else:
    #... initialized from pre-loaded file
    W1 = curW1
    W2 = curW2



#... set the training parameters
epochs = 5
num_samples = 2
learning_rate = 0.05
nll = 0
iternum = 0








# In[7]:


@jit
def sigmoid(x):
    return 1.0/(1+np.exp(-x))


# In[8]:


# @jit(nopython=True)
def performDescent(v ,learning_rate, center_token, context_token,W1,W2,negative_sample):
    #Log Likehood
    log_likelihood = 0
    for i in range(len(context_token)):
        if( i == 3):
            h = W1[center_token]
            vj_w2 = W2[context_token[i]]
            ##Log likehood
            log_likelihood = -np.log( sigmoid(np.dot( h , vj_w2)))
            for j in negative_sample[i]:
                vj_w2 = W2[j]
                log_likelihood = log_likelihood -  np.log(sigmoid( -np.dot(h , vj_w2) ))
        #rebalance

        h = W1[center_token]
        vj_w2 = W2[context_token[i]]
        result   =   sigmoid(np.dot(h , vj_w2)) - 1
        store_diff = result*vj_w2
        W2[context_token[i]] = W2[context_token[i]] - (learning_rate* result)*h
        for n in negative_sample[i]:
            vj_w2 =  W2[n]
            result   =  sigmoid(np.dot(h , vj_w2)) - 0
            store_diff = store_diff + result*vj_w2
            W2[n] = W2[n] - ((learning_rate* result)*h)

        #Update the context layer
        W1[center_token] = W1[center_token] - (learning_rate* store_diff)


    return (log_likelihood, W1,W2)





# In[ ]:
iter_num = -1
#... Begin actual training
for j in range(0,epochs):
    print ("Epoch: ", j)
    prevmark = 0

    i = start_point
    #... For each epoch, redo the whole sequence...
    while(i<end_point):
        iter_num = iter_num +1
        
        if (float(i)/len(mapped_sequence))>=(prevmark+0.1):
            print ("Progress: ", round(prevmark+0.1,1))
            prevmark += 0.1
        #... (TASK) determine which token is our current input. Remember that we're looping through mapped_sequence
        while(mapped_sequence[i] == "UNK"):
            i = i + 1
        #... now propagate to each of the context outputs
#         print(mapped_sequence[i])
        center_token = uniqueWords.index(mapped_sequence[i])
        context_index = []
        negative_indices = []
        for k in context_window:
#             print(k)
            #... (TASK) Use context_window to find one-hot index of the current context token.
            context_index_idx = mapped_sequence[i + k]#... fill in
            
            context_index.append(uniqueWords.index(context_index_idx))
            #... construct some negative samples
            negative_indices.append(generateSamples(context_index, num_samples))

            #... (TASK) You have your context token and your negative samples.
            #... Perform gradient descent on both weight matrices.
            #... Also keep track of the negative log-likelihood in variable nll.

             ## Let us run gradient descent on each
            # print(i)
        nll, W1,W2 = performDescent(i,learning_rate, center_token, context_index,W1,W2,negative_indices)
        nll = nll#[0][0]
        nll_results.append(nll)
            
        if (iter_num)%20000==0 and k==2 :
            n = np.sum(nll_results)
            nll_results_temp_all.append(n)
            print ("Negative likelihood: ", np.sum(nll_results))
            nll_results = []
        i= i+1

pickle.dump(W1, open("w1_v3.p","wb+"))
pickle.dump(W2, open("w2_v3.p","wb+"))
pickle.dump(nll_results_temp_all, open("nll_results.p","wb+"))







